import Config

config :flux_mqtt,
  broker: [
    host: "emq",
    port: 1883
  ],
  client: [
    name: "client",
    auth: [
      authenticate?: true,
      username: "client",
      password: "password"
    ],
    correlation: [
      create_correlation_id?: true,
      base: 36,
      length: 8,
      as: :suffix,
      separator: "_"
    ]
  ],
  initial_state: [],
  topics: []
