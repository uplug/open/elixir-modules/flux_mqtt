import Config

config :logger,
  backends: [:console],
  level: :debug,
  truncate: :infinity,
  discard_threshold: 5_000,
  handle_otp_reports: false

config :logger, :console,
  format: "[$level] $message    $metadata\n",
  metadata: [
    :flux_mqtt_detail,
    :flux_mqtt_error,
    :flux_mqtt_error_detail,
    :flux_mqtt_error_message,
    :flux_mqtt_message,
    :flux_mqtt_success_message
  ]
