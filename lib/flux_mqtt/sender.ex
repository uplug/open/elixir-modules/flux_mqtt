defmodule FluxMQTT.Sender do
  @moduledoc false

  @spec send(String.t(), String.t(), String.t(), keyword) ::
          :ok | {:ok, reference} | {:error, :unknown_connection}
  def send(payload, topic, client_id, opts \\ []) do
    Tortoise.publish(client_id, topic, payload, opts)
  end
end
