defmodule FluxMQTT.Connector do
  @moduledoc false

  alias FluxMQTT.Config
  alias FluxMQTT.Config.Client
  alias FluxMQTT.Config.Client.{Auth, Correlation}

  @spec connection(module, keyword) :: {Tortoise.Connection, keyword}
  def connection(handler, opts \\ []) do
    config =
      %Config{}
      |> Config.fetch(Application.get_all_env(:flux_mqtt))
      |> Config.fetch(opts)

    specification =
      handler
      |> build_connection_specification(config)
      |> maybe_add_authentication(config.client.auth)

    {Tortoise.Connection, specification}
  end

  defp build_connection_specification(handler, %Config{} = config) do
    client_id = build_client_id(config.client)

    [
      client_id: client_id,
      server: {
        Tortoise.Transport.Tcp,
        host: config.broker.host, port: config.broker.port
      },
      handler: {handler, [client_id: client_id] ++ config.initial_state},
      subscriptions: config.topics
    ]
  end

  defp maybe_add_authentication(specification, %Auth{} = auth) do
    if auth.authenticate? == true do
      [user_name: auth.username, password: auth.password] ++ specification
    else
      specification
    end
  end

  defp build_client_id(%Client{name: name, correlation: %Correlation{} = correlation}) do
    if correlation.create_correlation_id? == true do
      correlation_id = build_correlation_id(correlation)

      if correlation.as == :prefix do
        "#{correlation_id}#{correlation.separator}#{name}"
      else
        "#{name}#{correlation.separator}#{correlation_id}"
      end
    else
      name
    end
  end

  defp build_correlation_id(%Correlation{base: base, length: length}) do
    for _index <- 1..length do
      Integer.to_string(:rand.uniform(base), base)
    end
    |> Enum.join()
  end
end
