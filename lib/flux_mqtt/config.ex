defmodule FluxMQTT.Config do
  @moduledoc false

  alias FluxMQTT.Config
  alias FluxMQTT.Config.{Broker, Client}

  @type t :: %Config{
          broker: Broker.t(),
          client: Client.t(),
          initial_state: keyword,
          topics: list({String.t(), integer})
        }

  defstruct broker: %Broker{}, client: %Client{}, initial_state: [], topics: []

  @spec fetch(Config.t(), keyword) :: Config.t()
  def fetch(config \\ %Config{}, opts \\ []) do
    case opts do
      [] ->
        config

      opts ->
        struct(
          config,
          broker: Broker.fetch(config.broker, Keyword.get(opts, :broker, [])),
          client: Client.fetch(config.client, Keyword.get(opts, :client, [])),
          initial_state: Keyword.get(opts, :initial_state, []),
          topics: Keyword.get(opts, :topics, [])
        )
    end
  end
end
