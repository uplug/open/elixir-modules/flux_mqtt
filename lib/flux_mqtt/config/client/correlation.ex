defmodule FluxMQTT.Config.Client.Correlation do
  @moduledoc false

  alias FluxMQTT.Config.Client.Correlation

  @type t :: %Correlation{
          create_correlation_id?: boolean,
          base: integer,
          length: integer,
          as: :suffix | :prefix,
          separator: String.t()
        }

  defstruct create_correlation_id?: true, base: 32, length: 8, as: :suffix, separator: "_"

  @spec fetch(Correlation.t(), keyword) :: Correlation.t()
  def fetch(correlation \\ %Correlation{}, opts \\ []) do
    struct(correlation, opts)
  end
end
