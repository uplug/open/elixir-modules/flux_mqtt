defmodule FluxMQTT.Config.Client.Auth do
  @moduledoc false

  alias FluxMQTT.Config.Client.Auth

  @type t :: %Auth{
          authenticate?: boolean,
          username: String.t(),
          password: String.t()
        }

  defstruct authenticate?: true, username: "client", password: "password"

  @spec fetch(Auth.t(), keyword) :: Auth.t()
  def fetch(auth \\ %Auth{}, opts \\ []) do
    struct(auth, opts)
  end
end
