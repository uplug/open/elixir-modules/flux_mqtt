defmodule FluxMQTT.Config.Broker do
  @moduledoc false

  alias FluxMQTT.Config.Broker

  @type t :: %Broker{
          host: String.t(),
          port: integer
        }

  defstruct host: "emq", port: 1883

  @spec fetch(Broker.t(), keyword) :: Broker.t()
  def fetch(broker \\ %Broker{}, opts \\ []) do
    struct(broker, opts)
  end
end
