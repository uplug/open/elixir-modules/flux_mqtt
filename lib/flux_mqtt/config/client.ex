defmodule FluxMQTT.Config.Client do
  @moduledoc false

  alias FluxMQTT.Config.Client
  alias FluxMQTT.Config.Client.{Auth, Correlation}

  @type t :: %Client{
          name: String.t(),
          auth: Auth.t(),
          correlation: Correlation.t()
        }

  defstruct name: "client", auth: %Auth{}, correlation: %Correlation{}

  @spec fetch(Client.t(), keyword) :: Client.t()
  def fetch(client \\ %Client{}, opts \\ []) do
    case opts do
      [] ->
        client

      opts ->
        struct(
          client,
          name: Keyword.get(opts, :name, client.name),
          auth: Auth.fetch(client.auth, Keyword.get(opts, :auth, [])),
          correlation: Correlation.fetch(client.correlation, Keyword.get(opts, :correlation, []))
        )
    end
  end
end
