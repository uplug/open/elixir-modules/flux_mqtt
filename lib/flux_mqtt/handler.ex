defmodule FluxMQTT.Handler do
  @moduledoc """
  Module responsible for MQTT messages handling.

  The main approach to handle is done by defining a module which extends
  `FluxMQTT.Handler` and implements `c:handle/3` function:

  ```elixir
  defmodule MyApp.MQTT.Handler do
    use FluxMQTT.Handler

    @impl FluxMQTT.Handler
    def handle(topic, payload, state) do
      # Handle message
    end
  end
  ```

  Then, set the connection on the supervision tree:

  ```elixir
  defmodule MyApp.MQTT.Supervisor do
    use Supervisor

    alias MyApp.MQTT.Handler

    def start_link(arg), do: Supervisor.start_link(__MODULE__, arg, name: __MODULE__)

    @impl Supervisor
    def init(_arg) do
      children = [
        FluxMQTT.connection(Handler)
      ]

      opts = [strategy: :one_for_one]

      Supervisor.init(children, opts)
    end
  end
  ```

  Application:

  ```elixir
  defmodule MyApp.Application do
    use Application

    @impl Application
    def start(_type, _args) do
      children = [
        MyApp.MQTT.Supervisor
      ]

      opts = [strategy: :one_for_one]

      Supervisor.start_link(children, opts)
    end
  end
  ```

  ## Matching Dynamic Topics

  Sometimes the definition of topics cannot be determined programatically, such
  as when using environment variables to set the service topics.

  When a module uses `FluxMQTT.Handler`, the function `match_topic/3` is
  imported to the module. This function expects as parameters:

  - `received` - The topic received on `c:handle/3` function. Accepts a
    `t:list/0` of `t:String.t/0`.

  - `expected` - The topic defined dynamically. Accepts `t:String.t/0`.

  - `matcher` - Used to define the key names for the resultant map. Accepts
    `t:String.t/0`.

  Example:

  ```elixir
  defmodule MyApp.MQTT.Handler do
    use FluxMQTT.Handler

    @impl FluxMQTT.Handler
    def handle(topic, payload, state) do
      expected = "user/+/friend/+"
      matcher = "user/id/friend/friend_id"

      case match_topic(topic, expected, matcher) do
        {:ok, %{id: id, friend_id: friend_id}} -> # Use ids
        {:error, :unmatch} -> # Not the expected topic
      end
    end
  end
  ```
  """

  @moduledoc since: "0.0.1"

  @callback handle(
              topic :: list(String.t()),
              payload :: String.t(),
              state :: keyword
            ) :: any

  defmacro __using__(_opts) do
    quote do
      use Tortoise.Handler

      require Logger

      @behaviour FluxMQTT.Handler

      @impl Tortoise.Handler
      def init(args), do: {:ok, args}

      @impl Tortoise.Handler
      def connection(_status, state) do
        Logger.info("Established connection with MQTT broker",
          flux_mqtt_detail: inspect(state: state)
        )

        {:ok, state}
      end

      @impl Tortoise.Handler
      def handle_message(topic, payload, state) do
        Logger.debug("MQTT message received",
          flux_mqtt_detail: inspect(topic: topic, state: state),
          flux_mqtt_message: inspect(payload)
        )

        spawn(fn -> handle(topic, payload, state) end)

        {:ok, state}
      end

      @impl Tortoise.Handler
      def subscription(_status, _topic_filter, state), do: {:ok, state}

      @impl Tortoise.Handler
      def terminate(reason, _state) do
        Logger.warn("MQTT connection closed", flux_mqtt_error: inspect(reason))

        :ok
      end

      defp match_topic(received, expected, matcher) do
        expected = String.split(expected, "/")

        if length(received) == length(expected) do
          matcher = String.split(matcher, "/")
          {:ok, Enum.reduce(Enum.zip([expected, matcher, received]), %{}, &extract_params/2)}
        else
          raise "unmatch"
        end
      rescue
        _error -> {:error, :unmatch}
      end

      defp extract_params({base, param, value}, map) do
        cond do
          base == value ->
            map

          base == "+" ->
            if param != "+" do
              Map.put(map, String.to_atom(param), value)
            else
              map
            end

          true ->
            raise "unmatch"
        end
      end
    end
  end
end
