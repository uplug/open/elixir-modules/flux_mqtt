defmodule FluxMQTT.DocTest do
  use ExUnit.Case, async: true

  @moduletag :capture_log

  doctest FluxMQTT
end
