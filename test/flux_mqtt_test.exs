defmodule FluxMQTTTest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureLog

  alias FluxMQTT.Handler

  describe "connection/2" do
    test "returns a valid Tortoise specification" do
      operation = fn ->
        {
          Tortoise.Connection,
          [
            user_name: username,
            password: password,
            client_id: client_id,
            server: {
              Tortoise.Transport.Tcp,
              [
                host: host,
                port: port
              ]
            },
            handler: {handler, [client_id: client_id]},
            subscriptions: subscriptions
          ]
        } = FluxMQTT.connection(Handler)

        assert client_id =~ "client_"
        assert username == "client"
        assert password == "password"
        assert host == "emq"
        assert port == 1883
        assert handler == Handler
        assert subscriptions == []
      end

      assert capture_log(operation) == ""
    end

    test "returns a valid Tortoise specification with initial state" do
      opts = [
        initial_state: [cat: :meow],
        topics: [{"$queue/a/topic", 0}]
      ]

      operation = fn ->
        {
          Tortoise.Connection,
          [
            user_name: username,
            password: password,
            client_id: client_id,
            server: {
              Tortoise.Transport.Tcp,
              [
                host: host,
                port: port
              ]
            },
            handler: {handler, [client_id: client_id, cat: cat]},
            subscriptions: subscriptions
          ]
        } = FluxMQTT.connection(Handler, opts)

        assert client_id =~ "client_"
        assert username == "client"
        assert password == "password"
        assert host == "emq"
        assert port == 1883
        assert handler == Handler
        assert subscriptions == [{"$queue/a/topic", 0}]
        assert cat == :meow
      end

      assert capture_log(operation) == ""
    end

    test "returns a valid Tortoise specification with prefixed correlation id and no auth" do
      opts = [
        client: [
          auth: [authenticate?: false],
          correlation: [as: :prefix]
        ],
        topics: [{"$queue/a/topic", 0}]
      ]

      operation = fn ->
        {
          Tortoise.Connection,
          [
            client_id: client_id,
            server: {
              Tortoise.Transport.Tcp,
              [
                host: host,
                port: port
              ]
            },
            handler: {handler, [client_id: client_id]},
            subscriptions: subscriptions
          ]
        } = FluxMQTT.connection(Handler, opts)

        assert client_id =~ "_client"
        assert host == "emq"
        assert port == 1883
        assert handler == Handler
        assert subscriptions == [{"$queue/a/topic", 0}]
      end

      assert capture_log(operation) == ""
    end

    test "returns a valid Tortoise specification without correlation_id" do
      opts = [
        client: [correlation: [create_correlation_id?: false]],
        topics: [{"$queue/a/topic", 0}]
      ]

      operation = fn ->
        {
          Tortoise.Connection,
          [
            user_name: username,
            password: password,
            client_id: client_id,
            server: {
              Tortoise.Transport.Tcp,
              [
                host: host,
                port: port
              ]
            },
            handler: {handler, [client_id: client_id]},
            subscriptions: subscriptions
          ]
        } = FluxMQTT.connection(Handler, opts)

        assert client_id == "client"
        assert username == "client"
        assert password == "password"
        assert host == "emq"
        assert port == 1883
        assert handler == Handler
        assert subscriptions == [{"$queue/a/topic", 0}]
      end

      assert capture_log(operation) == ""
    end
  end

  describe "send/4" do
    test "returns {:error, :unknown_connection} if no connection is configured" do
      operation = fn ->
        assert FluxMQTT.send("test", "a/topic", "meow") == {:error, :unknown_connection}
      end

      assert capture_log(operation) =~ "MQTT message sending failed"
    end
  end
end
