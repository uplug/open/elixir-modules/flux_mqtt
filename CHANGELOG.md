# Changelog

## 0.0.4 - 2019-10-07

### Changed

- Logger metadata changed to:

  1. `:flux_mqtt_detail` - The context detailed values.
  2. `:flux_mqtt_error` - The error result of the context.
  3. `:flux_mqtt_error_detail` - The error context detailed values.
  4. `:flux_mqtt_error_message` - The MQTT message received, referenced in
    error logs.
  5. `:flux_mqtt_message` - The MQTT message received.
  6. `:flux_mqtt_success_message` - The MQTT message received, referenced in
    success logs.

## 0.0.3 - 2019-09-03

### Changed

- Callback `c:FluxMQTT.Handler.handle/3` return type changed from `t:atom/0`
  to `t:any/0`.

## 0.0.2 - 2019-09-01

### Changed

- Every logger now uses metadata to describe information. There are three
  metadata keys that can be filtered on logger settings:

  1. `:flux_mqtt_params` - The params received by function.
  2. `:flux_mqtt_result` - The result of the function.
  3. `:error` - The error result of the function.

## 0.0.1 - 2019-08-31

### Added

- The project is available on GitLab and Hex.

### Notes

- Minor changes (0.0.x) from the current version will be logged to this file.

- When a major change is released (0.x.0 or x.0.0), the changelog of the
  previous major change will be grouped as a single change.
