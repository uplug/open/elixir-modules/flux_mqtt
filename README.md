# Flux MQTT

[![pipeline status](https://gitlab.com/uplug/open/elixir-modules/flux_mqtt/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_mqtt/commits/master)
[![coverage report](https://gitlab.com/uplug/open/elixir-modules/flux_mqtt/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_mqtt/commits/master)

An interface to connect to MQTT broker, sending and handling messages.

It uses `Tortoise`, check their documentation to understand how this library
perform the configuration, handling, and delivery of MQTT messages.

## Usage

Add [Flux MQTT](https://hex.pm/packages/flux_mqtt) as a dependency in your
`mix.exs` file:

```elixir
def deps do
  [{:flux_mqtt, "~> 0.0.3"}]
end
```

`FluxMQTT` describes how to define a connection specification and how to send a message.

`FluxMQTT.Handler` describes how to handle MQTT messages defined in a connection.

## Application Configuration

```elixir
import Config

# Default values
config :flux_mqtt,
  broker: [
    host: "emq",
    port: 1883
  ],
  client: [
    name: "client",
    auth: [
      authenticate?: true,
      username: "client",
      password: "password"
    ],
    correlation: [
      create_correlation_id?: true,
      base: 36,
      length: 8,
      as: :suffix,
      separator: "_"
    ]
  ],
  initial_state: [],
  topics: []
```

### Configuration Options

- `:broker` - Set the MQTT broker. Accepts `t:keyword/0`. Options are:

  - `:host` - The MQTT broker hostname. Defaults to `emq`. Accepts
    `t:String.t/0`.

  - `:port` - The MQTT broker port. Defaults to `1883`. Accepts `t:integer/0`.

- `:client` - Set the service settings as a broker client. Accepts
  `t:keyword/0`. Options are:

  - `:name` - The name of the client. Defaults to `client`. It is suggested to
    set the name as the name of the service. Accepts `t:String.t/0`.

  - `:auth` - Set broker authentication settings. Accepts `t:keyword/0`. Options are:

    - `:authenticate?` - If `true`, define the connection specification with
      `username` and `password` (see below). Defaults to `true`. Accepts
      `t:boolean/0`.

    - `:username` - The username to be used as service authentication.
      Defaults to `client`. Accepts `t:String.t/0`.

    - `:password` - The username's password. Defaults to `password`. Accepts
      `t:String.t/0`.

  - `:correlation` - Set the client correlation settings. Accepts
    `t:keyword/0`. Options are:

    - `:create_correlation_id?` - If `true`, the service `client_id` will be
      defined with `client.name` and a correlation string. Defaults to `true`.
      Accepts `t:boolean/0`.

    - `:base` - The base to be used when generating a correlation id. Must have a
      value in the range `2..36`. Defaults to `36`. Accepts `t:integer/0`.

    - `:length` - How many characters will be defined for the correlation id.
      Defaults to `8`. Accepts `t:integer/0`.

    - `:as` - `:suffix` sets the correlation id before the client name. `:prefix`
      sets the correlation id after the client name. Defaults to `:suffix`.
      Accepts `:suffix` or `:prefix`.

    - `:separator` - The separator between the correlation id and the client name.
      Defaults to `_`. Accepts `t:String.t/0`.

- `:initial_state` - A keyword list with data to be set as the handler initial
  state. Defaults to an empty list. Accepts `t:keyword/0`.

- `:topics` - A list of `{topic, qos}` elements from which the handler will
  receive messages. Defaults to an empty list. Check `t:Tortoise.qos/0` and
  `t:Tortoise.topic/0` for more information. Accepts a `t:list/0` of
  `t:tuple/0`.
