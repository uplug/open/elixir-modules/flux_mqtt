defmodule FluxMQTT.MixProject do
  use Mix.Project

  @version "0.0.4"
  @source_url "https://gitlab.com/uplug/open/elixir-modules/flux_mqtt"

  def project do
    [
      app: :flux_mqtt,
      name: "Flux MQTT",
      description: "An interface to connect to MQTT broker, sending and handling messages.",
      source_url: @source_url,
      version: @version,
      elixir: "~> 1.9",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      preferred_cli_env: preferred_cli_env(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      deps: deps(),
      docs: docs(),
      package: package()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      "test.all": ["test.static", "test.coverage"],
      "test.coverage": ["coveralls"],
      "test.static": ["format --check-formatted", "credo list --strict --all"]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.1.3", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21.2", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.11.2", only: [:dev, :test]},
      {:tortoise, "~> 0.9.4"}
    ]
  end

  defp docs do
    [
      main: "readme",
      authors: ["Jonathan Moraes", "Lucas Alves"],
      extras: ~w(CHANGELOG.md README.md)
    ]
  end

  defp package do
    [
      maintainers: ["Eduardo Pinheiro", "Jonathan Moraes", "Lucas Alves"],
      licenses: ["Apache-2.0"],
      files: ~w(lib mix.exs CHANGELOG.md LICENSE README.md),
      links: %{"GitLab" => @source_url}
    ]
  end

  defp preferred_cli_env do
    [
      coveralls: :test,
      "coveralls.detail": :test,
      "coveralls.html": :test,
      "coveralls.post": :test,
      "test.all": :test,
      "test.coverage": :test,
      "test.static": :test
    ]
  end
end
